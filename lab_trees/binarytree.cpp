/**
 * @file binarytree.cpp
 * Definitions of the binary tree functions you'll be writing for this lab.
 * You'll need to modify this file.
 */
#include "TreeTraversals/InorderTraversal.h"
#include <iostream>
/**
 * @return The height of the binary tree. Recall that the height of a binary
 *  tree is just the length of the longest path from the root to a leaf, and
 *  that the height of an empty tree is -1.
 */
template <typename T>
int BinaryTree<T>::height() const
{
    // Call recursive helper function on root
    return height(root);
}

/**
 * Private helper function for the public height function.
 * @param subRoot
 * @return The height of the subtree
 */
template <typename T>
int BinaryTree<T>::height(const Node* subRoot) const
{
    // Base case
    if (subRoot == NULL)
        return -1;

    // Recursive definition
    return 1 + max(height(subRoot->left), height(subRoot->right));
}

/**
 * Prints out the values of the nodes of a binary tree in order.
 * That is, everything to the left of a node will be printed out before that
 * node itself, and everything to the right of a node will be printed out after
 * that node.
 */
template <typename T>
void BinaryTree<T>::printLeftToRight() const
{
    // Call recursive helper function on the root
    printLeftToRight(root);

    // Finish the line
    cout << endl;
}

/**
 * Private helper function for the public printLeftToRight function.
 * @param subRoot
 */
template <typename T>
void BinaryTree<T>::printLeftToRight(const Node* subRoot) const
{
    // Base case - null node
    if (subRoot == NULL)
        return;

    // Print left subtree
    printLeftToRight(subRoot->left);

    // Print this node
    cout << subRoot->elem << ' ';

    // Print right subtree
    printLeftToRight(subRoot->right);
}

/**
* Flips the tree over a vertical axis, modifying the tree itself
*  (not creating a flipped copy).
*/
template <typename T>
void BinaryTree<T>::mirror()
{
    mirror(root);
}

template <typename T>
void BinaryTree<T>::mirror(Node* subRoot)
{
    // Base case - null node
    if (subRoot == NULL)
        return;

    // Mirror left subtree
    mirror(subRoot->left);

    // Mirror right subtree
    mirror(subRoot->right);

    // Mirror this node
    Node* temp = subRoot->left;
    subRoot->left = subRoot->right;
    subRoot->right = temp;
}

/**
 * isOrdered() function iterative version
 * @return True if an in-order traversal of the tree would produce a
 *  nondecreasing list output values, and false otherwise. This is also the
 *  criterion for a binary tree to be a binary search tree.
 */
template <typename T>
bool BinaryTree<T>::isOrderedIterative() const
{
    InorderTraversal<T> iot(root);
    typename TreeTraversal<T>::Iterator it = iot.begin();
    T previous = (*it)->elem;
    ++it;
    while (it != iot.end()) {
        T current = (*it)->elem;
        if (current < previous) {
            return false;
        }
        previous = current;
        ++it;
    }
    return true;
}

/**
 * isOrdered() function recursive version
 * @return True if an in-order traversal of the tree would produce a
 *  nondecreasing list output values, and false otherwise. This is also the
 *  criterion for a binary tree to be a binary search tree.
 */
template <typename T>
bool BinaryTree<T>::isOrderedRecursive() const
{
    return (isOrderedRecursive(root, NULL, NULL));
}

template <typename T>
bool BinaryTree<T>::isOrderedRecursive(Node* subRoot, Node* left, Node* right) const
{
    if (subRoot == NULL) return true;

    if ((left != NULL) && (subRoot->elem < left->elem)) {
        return false;
    }

    if ((right != NULL) && (subRoot->elem > right->elem)) {
        return false;
    }

    return (isOrderedRecursive(subRoot->left, left, subRoot) &&
            isOrderedRecursive(subRoot->right, subRoot, right));
}

/**
 * creates vectors of all the possible paths from the root of the tree to any leaf
 * node and adds it to another vector.
 * Path is, all sequences starting at the root node and continuing
 * downwards, ending at a leaf node. Paths ending in a left node should be
 * added before paths ending in a node further to the right.
 * @param paths vector of vectors that contains path of nodes
 */
template <typename T>
void BinaryTree<T>::printPaths(vector<vector<T> > &paths) const
{
    vector<T> currPath;
    pathHelper(paths, currPath, root);

}

template <typename T>
void BinaryTree<T>::pathHelper(vector<vector<T> > &paths, vector<T> currPath, Node* subRoot) const
{
    // base case: null node
    if (subRoot == NULL) {
        return;
    }

    // append this node to the path vector
    currPath.push_back(subRoot->elem);

    // if this node is a leaf, append the list
    // to the list of lists
    if (!subRoot->left && !subRoot->right) {
        paths.push_back(currPath);
    }
    else {
        pathHelper(paths, currPath, subRoot->left);
        pathHelper(paths, currPath, subRoot->right);
    }
}

/**
 * Each node in a tree has a distance from the root node - the depth of that
 * node, or the number of edges along the path from that node to the root. This
 * function returns the sum of the distances of all nodes to the root node (the
 * sum of the depths of all the nodes). Your solution should take O(n) time,
 * where n is the number of nodes in the tree.
 * @return The sum of the distances of all nodes to the root
 */
template <typename T>
int BinaryTree<T>::sumDistances() const
{
    int count = 0;
    return distanceHelper(root, 0, count) - count;
}

template <typename T>
int BinaryTree<T>::distanceHelper(Node* subRoot, int dist, int & count) const
{
    if (subRoot == NULL) {
        return 0;
    }
    count ++;
    dist ++;

    int left = distanceHelper(subRoot->left, dist, count);
    int right = distanceHelper(subRoot->right, dist, count);

    return dist + left + right;
}
