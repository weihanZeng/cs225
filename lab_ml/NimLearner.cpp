/**
 * @file NimLearner.cpp
 * CS 225: Data Structures
 */

#include "NimLearner.h"
#include <ctime>

using std::to_string;

/**
 * Constructor to create a game of Nim with `startingTokens` starting tokens.
 *
 * This function creates a graph, `g_` representing all of the states of a
 * game of Nim with vertex labels "p#-X", where:
 * - # is the current player's turn; p1 for Player 1, p2 for Player2
 * - X is the tokens remaining at the start of a player's turn
 *
 * For example:
 *   "p1-4" is Player 1's turn with four (4) tokens remaining
 *   "p2-8" is Player 2's turn with eight (8) tokens remaining
 *
 * All legal moves between states are created as edges with initial weights
 * of 0.
 *
 * @param startingTokens The number of starting tokens in the game of Nim.
 */
NimLearner::NimLearner(unsigned startingTokens) : g_(true, true) {
    for (unsigned t = 0; t <= startingTokens; t ++) {
		Vertex p1 = g_.insertVertexByLabel("p1-" + to_string(t));
		Vertex p2 = g_.insertVertexByLabel("p2-" + to_string(t));

        if (t >= 1) {
            Vertex p1_prev = g_.getVertexByLabel("p1-" + to_string(t-1));
            Vertex p2_prev = g_.getVertexByLabel("p2-" + to_string(t-1));
            g_.insertEdge(p1, p2_prev);
            g_.insertEdge(p2, p1_prev);
            g_.setEdgeWeight(p1, p2_prev, 0);
            g_.setEdgeWeight(p2, p1_prev, 0);
        }
        if (t >= 2) {
            Vertex p1_prev2 = g_.getVertexByLabel("p1-" + to_string(t-2));
            Vertex p2_prev2 = g_.getVertexByLabel("p2-" + to_string(t-2));
            g_.insertEdge(p1, p2_prev2);
            g_.insertEdge(p2, p1_prev2);
            g_.setEdgeWeight(p1, p2_prev2, 0);
            g_.setEdgeWeight(p2, p1_prev2, 0);
        }
	}
    // save the starting vertex for later
    startingVertex_ = g_.getVertexByLabel("p1-" + to_string(startingTokens));
}

/**
 * Plays a random game of Nim, returning the path through the state graph
 * as a vector of `Edge` classes.  The `origin` of the first `Edge` must be
 * the vertex with the label "p1-#", where # is the number of starting
 * tokens.  (For example, in a 10 token game, result[0].origin must be the
 * vertex "p1-10".)
 *
 * @returns A random path through the state space graph.
 */
std::vector<Edge> NimLearner::playRandomGame() const {
    vector<Edge> path;

    // start with vertex P1-#, where # is startingTokens
    Vertex curr = startingVertex_;

    // get the available nodes to visit
    vector<Vertex> adj = g_.getAdjacent(curr);

    // while we have nodes to visit, choose one and repeat
    while (!adj.empty()) {
        // randomly chose one edge to take
        Vertex next = adj[rand() % adj.size()];
        // put the chosen edge on the path vector
        path.push_back(g_.getEdge(curr, next));
        // update current and adjacency list
        curr = next;
        adj = g_.getAdjacent(curr);
    }

    return path;
}

/*
 * Updates the edge weights on the graph based on a path through the state
 * tree.
 *
 * If the `path` has Player 1 winning (eg: the last vertex in the path goes
 * to Player 2 with no tokens remaining, or "p2-0", meaning that Player 1
 * took the last token), then all choices made by Player 1 (edges where
 * Player 1 is the source vertex) are rewarded by increasing the edge weight
 * by 1 and all choices made by Player 2 are punished by changing the edge
 * weight by -1.
 *
 * Likewise, if the `path` has Player 2 winning, Player 2 choices are
 * rewarded and Player 1 choices are punished.
 *
 * @param path A path through the a game of Nim to learn.
 */
void NimLearner::updateEdgeWeights(const std::vector<Edge> & path) {
    Vertex endState = path[path.size() - 1].dest;

    bool player1Wins = (g_.getVertexLabel(endState) == "p2-0");

    for (Edge e : path) {
        int weight = g_.getEdgeWeight(e.source, e.dest);
        bool player1Move = (g_.getVertexLabel(e.source).substr(0,2) == "p1");

        // update the edge weight
        // If player 1 made the move, increase the weight if player 1 also wins, otherwise decrese
        // else, (it is player 2's move) decrease the weight if player 1 wins otherwise increase.
        weight += (player1Move ? (player1Wins ? 1 : -1) : (player1Wins ? -1 : 1));
        g_.setEdgeWeight(e.source, e.dest, weight);
    }
}

/**
 * Label the edges as "WIN" or "LOSE" based on a threshold.
 */
void NimLearner::labelEdgesFromThreshold(int threshold) {
    for (const Vertex & v : g_.getVertices()) {
        for (const Vertex & w : g_.getAdjacent(v)) {
            int weight = g_.getEdgeWeight(v, w);

            // Label all edges with positve weights as "WINPATH"
            if (weight > threshold)           { g_.setEdgeLabel(v, w, "WIN"); }
            else if (weight < -1 * threshold) { g_.setEdgeLabel(v, w, "LOSE"); }
        }
    }
}

/**
 * Returns a constant reference to the state space graph.
 *
 * @returns A constant reference to the state space graph.
 */
const Graph & NimLearner::getGraph() const {
    return g_;
}
