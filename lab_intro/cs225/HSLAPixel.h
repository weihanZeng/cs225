/**
 * @file HSLAPixel.h
 *
 * @author CS 225: Data Structures
 * @version 2018r1-lab1
 */

#ifndef CS225_HSLAPIXEL_H_
#define CS225_HSLAPIXEL_H_

#include <iostream>
#include <sstream>

namespace cs225 {
class HSLAPixel {
  public:
    HSLAPixel();
    HSLAPixel(double hue, double saturation, double luminance);
    HSLAPixel(double hue, double saturation, double luminance, double alpha);

    double h;   // hue of the pixel, in degrees [0,360)
    double s;   // saturation of the pixel, [0,1]
    double l;   // luminance of the pixel, [0,1]
    double a;   // alpha of the pixel, [0,1]

};
} // end cs225 namespace

#endif
