#include <iostream>
#include <cmath>
#include <cstdlib>

#include "cs225/PNG.h"
#include "cs225/HSLAPixel.h"
#include "lab_intro.h"

using namespace cs225;

/**
 * Returns an image that has been transformed to grayscale.
 *
 * The saturation of every pixel is set to 0, removing any color.
 *
 * @return The grayscale image.
 */
PNG grayscale(PNG image) {
    /// This function is already written for you so you can see how to
    /// interact with our PNG class.
    for (unsigned x = 0; x < image.width(); x++) {
        for (unsigned y = 0; y < image.height(); y++) {
            HSLAPixel & pixel = image.getPixel(x, y);

            // `pixel` is a pointer to the memory stored inside of the PNG `image`,
            // which means you're changing the image directly.  No need to `set`
            // the pixel since you're directly changing the memory of the image.
            pixel.s = 0;
        }
    }

    return image;
}



/**
 * Returns an image with a spotlight centered at (`centerX`, `centerY`).
 *
 * A spotlight adjusts the luminance of a pixel based on the distance the pixel
 * is away from the center by decreasing the luminance by 0.5% per 1 pixel euclidean
 * distance away from the center.
 *
 * For example, a pixel 3 pixels above and 4 pixels to the right of the center
 * is a total of `sqrt((3 * 3) + (4 * 4)) = sqrt(25) = 5` pixels away and
 * its luminance is decreased by 2.5% (0.975x its original value).  At a
 * distance over 160 pixels away, the luminance will always decreased by 80%.
 *
 * The modified PNG is then returned.
 *
 * @param image A PNG object which holds the image data to be modified.
 * @param centerX The center x coordinate of the crosshair which is to be drawn.
 * @param centerY The center y coordinate of the crosshair which is to be drawn.
 *
 * @return The image with a spotlight.
 */
PNG createSpotlight(PNG image, int centerX, int centerY) {
    for (unsigned x = 0; x < image.width(); x++) {
        for (unsigned y = 0; y < image.height(); y++) {
            HSLAPixel & pixel = image.getPixel(x, y);

            int xDistance = x - centerX;
            int yDistance = y - centerY;
            double distance = sqrt((xDistance * xDistance) + (yDistance * yDistance));

            if (distance < 160) {
                // decrease this pixel's luminance by .5% (=0.005) for each unit of distance
                pixel.l *= (1 - (0.005 * distance));
            }
            else {
                // if the distance is >= 160 pixels, simply cut the luminance to 20%
                pixel.l *= 0.20;
            }
        }
    }

    return image;
}


/**
 * Returns a image transformed to Illini colors.
 *
 * The hue of every pixel is set to the a hue value of either orange or
 * blue, based on if the pixel's hue value is closer to orange than blue.
 *
 * @param image A PNG object which holds the image data to be modified.
 *
 * @return The illinify'd image.
**/
PNG illinify(PNG image) {
    // Illini Orange: RGB(232, 74, 39), HSL( 11, 80.8, 53.1)
    // Illini Blue:   RGB( 19, 41, 75), HSL(216, 59.6, 18.4)
    for (unsigned x = 0; x < image.width(); x++) {
        for (unsigned y = 0; y < image.height(); y++) {
            HSLAPixel & pixel = image.getPixel(x, y);

            // Illini Orange and Blue have hue values of 11 and 216, respectively.
            // Because hue is a circular scale, there are two points which are halfway
            // between these values. The first is the standard midpoint, 11 + (216-11)/2 = 113.5
            // For the second, treat 11 as 360+11 = 341. Then the midpoint is 216 + (341-216)/2 = 278.5
            // So a hue is closer to Illini Blue than orange if it is in the range [113.5,278.5)
            // and closer to orange otherwise.
            if (pixel.h >= 113.5 && pixel.h < 278.5) {
                pixel.h = 216;
            }
            else {
                pixel.h = 11;
            }
        }
    }

    return image;
}


/**
* Returns an immge that has been watermarked by another image.
*
* The luminance of every pixel of the second image is checked, if that
* pixel's luminance is 1 (100%), then the pixel at the same location on
* the first image has its luminance increased by 0.2.
*
* @param firstImage  The first of the two PNGs to be averaged together.
* @param secondImage The second of the two PNGs to be averaged together.
*
* @return The watermarked image.
*/
PNG watermark(PNG firstImage, PNG secondImage) {
    unsigned maxWidth = max(firstImage.width(), secondImage.width());
    unsigned maxHeight = max(firstImage.height(), secondImage.height());
    for (unsigned x = 0; x < maxWidth; x++) {
        for (unsigned y = 0; y < maxHeight; y++) {
            HSLAPixel & firstPixel = firstImage.getPixel(x, y);
            HSLAPixel & secondPixel = secondImage.getPixel(x, y);

            if (secondPixel.l == 1.0) {
                firstPixel.l = min(firstPixel.l + 0.2, 1.0);
            }
        }
    }

    return firstImage;
}
