/**
 * @file common_words.cpp
 * Implementation of the CommonWords class.
 *
 * @author Zach Widder
 * @date Fall 2014
 */

#include "common_words.h"

#include <fstream>
#include <string>
#include <vector>
#include <iostream>
#include <iterator>
#include <algorithm>

using std::string;
using std::vector;
using std::ifstream;
using std::cout;
using std::endl;
using std::feof;

string remove_punct(const string& str)
{
    string ret;
    std::remove_copy_if(str.begin(), str.end(), std::back_inserter(ret),
                        std::ptr_fun<int, int>(&std::ispunct));
    return ret;
}

CommonWords::CommonWords(const vector<string>& filenames)
{
    // initialize all member variables
    init_file_word_maps(filenames);
    init_common();
}

void CommonWords::init_file_word_maps(const vector<string>& filenames)
{
    // make the length of file_word_maps the same as the length of filenames
    file_word_maps.resize(filenames.size());

    // go through all files
    for (size_t i = 0; i < filenames.size(); i++) {
        // get the corresponding vector of words that represents the current
        // file
        vector<string> words = file_to_vector(filenames[i]);
        map<string, unsigned> word_counts;
		for (string word : words) {
			word_counts[word] ++;
		}
		file_word_maps[i] = word_counts;
    }
}

void CommonWords::init_common()
{
    for (size_t i = 0; i < file_word_maps.size(); i ++) {
		auto word_map = file_word_maps[i];
		for (auto pair : word_map) {
			common[pair.first] ++;
		}
	}
}

/**
 * @param n The number of times to word has to appear.
 * @return A vector of strings. The vector contains all words that appear
 * in each file >= n times.
 */
vector<string> CommonWords::get_common_words(unsigned int n) const
{
    vector<string> out;

	for (auto pair : common) {
		string word = pair.first;

		// make sure this word is in every file
		if (pair.second < file_word_maps.size())
			continue;

		// for each file, find how many times the word appears
		bool is_common = true;
		for (const auto& word_map : file_word_maps) {
			// if the word wasn't in this file, or if it was but it didn't
			// appear n times, then it doesn't meet the criteria
			if (word_map.find(word) == word_map.end() ||
				word_map.at(word) < n) {
					is_common = false;
					break;
			}
		}

		// if we didn't hit a case to say otherwise,
		// include this word as common
		if (is_common) {
			out.push_back(word);
		}
	}
    return out;
}

/**
 * Takes a filename and transforms it to a vector of all words in that file.
 * @param filename The name of the file that will fill the vector
 */
vector<string> CommonWords::file_to_vector(const string& filename) const
{
    ifstream words(filename);
    vector<string> out;

    if (words.is_open()) {
        std::istream_iterator<string> word_iter(words);
        while (!words.eof()) {
            out.push_back(remove_punct(*word_iter));
            ++word_iter;
        }
    }
	words.close();
    return out;
}
