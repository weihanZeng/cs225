#include <random>

#include "../cs225/HSLAPixel.h"
#include "../Point.h"

#include "ColorPicker.h"
#include "MyColorPicker.h"

using namespace cs225;

MyColorPicker::MyColorPicker(HSLAPixel centerColor, unsigned stdDev)
    : centerColor(centerColor)
{
    distribution = std::normal_distribution<double>(centerColor.h, stdDev);
}


/**
 * Picks the color for pixel (x, y).
 * Using your own algorithm
 */
HSLAPixel MyColorPicker::getColor(unsigned x, unsigned y) {
    /* @todo [Part 3] */
    double hue = distribution(generator);
    if (hue > 360.0) hue -= 360.0;
    if (hue < 0.0) hue += 360.0;
    return HSLAPixel(hue, 1.0, 0.5);
}
