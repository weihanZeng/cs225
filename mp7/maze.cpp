#include <algorithm>
#include <vector>
#include <queue>
#include <cstdlib>
#include <sys/time.h>
#include <time.h>
#include <cmath>
#include "maze.h"
#include "dsets.h"

SquareMaze::SquareMaze() {

}

void SquareMaze::makeMaze(int width, int height) {
    this->width = width;
    this->height = height;

    int numCells = width * height;

    // create a vector to store which walls are present for
    // each cell. Cleared before resizing in case the maze
    // is regenerated. Stored as a 1D array for simplicity
    walls.clear();
    walls.resize(numCells, 3);

    // create a disjoint set for each cell
    DisjointSets cells;
    cells.addelements(numCells);

    // seed the random number generator using the system time
    // srand(time(NULL));

    // while multiple disjoint sets exist, randomly choose
    // walls to remove
    while (cells.size(0) < numCells) {
        // randomly pick a cell and a direction
        // (right or down), then calculate the
        // adjacent cell index in the chosen direction
        int cell1 = rand() % (numCells - 1);  // the -1 ensures we do not pick the bottom right corner
        int dir;
        // if cell1 is on the right edge, chose cell2 below it
        // if cell1 is on the bottom edge, choose cell2 next to it
        // otherwise chose randomly
        if (cell1 % width == width - 1)       { dir = 1; }
        else if (cell1 / width == height - 1) { dir = 0; }
        else                                  { dir = rand() % 2; }
        // set index of cell2 accordingly
        int cell2 = cell1 + ((dir == RIGHT) ? 1 : width);

        // We now have two indices of adjacent cells, cell1 and cell2.
        // If they are in the same set, then we must not remove any walls,
        // otherwise remove the wall indicated by dir
        if (cells.find(cell1) != cells.find(cell2)) {
            cells.setunion(cell1, cell2);
            walls[cell1] &= (dir == RIGHT) ? 0b10 : 0b01;
        }
    }
}

bool SquareMaze::canTravel(int x, int y, int dir) const {
    // if checking left/up, adjust the
    // coordinate so the math is the same
    if (dir == LEFT) x --;
    if (dir == UP) y --;

    // quick bounds check to make sure we're on the maze
    if (x >= width || x < 0 || y >= height || y < 0)
        return false;

    // create a bitmask to check the presence of a wall in the specified direction
    // (dir & DOWN) will be 1 for UP/DOWN, 0 for RIGHT/LEFT
    int mask = (dir & DOWN) ? 0b10 : 0b01;
    // check the walls vector by applying the mask
    // will be zero if the wall is not present, so return the oposite
    return !(walls[y*width + x] & mask);
}

void SquareMaze::setWall(int x, int y, int dir, bool exists) {
    /** RIGHTWALL = 1, DOWNWALL = 2, BOTHWALLS = 3 **/

    // setting the wall -> bitwise OR with 1 in position of DIR
    if (exists) {
        walls[y*width + x] |= (dir == RIGHT) ? 0b01 : 0b10;
    }
    // clearing the wall -> bitwise AND with 0 in position of DIR
    else {
        walls[y*width + x] &= (dir == RIGHT) ? 0b10 : 0b01;
    }
}

vector<int> SquareMaze::solveMaze() {
    vector<int> solution;

    // matrix of visited cells
    vector<vector<bool>> v(height, vector<bool>(width,false));
    // matrix of cell distance from origin
    vector<vector<int>>  d(height, vector<int>(width,0));
    // matrix of previous cell coordinates
    vector<vector<Point>> p(height, vector<Point>(width, Point(-1,-1)));

    queue<Point> q;
    v[0][0] = true;
    q.push(Point(0,0));

    while (!q.empty()) {
        Point cell = q.front(); q.pop();
        int x = cell.x;
        int y = cell.y;
        int dist = d[y][x];

        // check for adjacent cells -> right, down, left, up
        vector<Point> adj;
        if (canTravel(x, y, RIGHT)) adj.push_back(Point(x + 1, y));
        if (canTravel(x, y, DOWN))  adj.push_back(Point(x, y + 1));
        if (canTravel(x, y, LEFT))  adj.push_back(Point(x - 1, y));
        if (canTravel(x, y, UP))    adj.push_back(Point(x, y - 1));

        // for each adjacent cell
        for (Point c : adj) {
            x = c.x;
            y = c.y;
            // if it hasn't been visited
            if (v[y][x] == false) {
                p[y][x] = cell;         // set the previous coordinate,
                d[y][x] = dist + 1;     // increment the distance,
                v[y][x] = true;         // record that we visited,
                q.push(c);              // and add to the BFS queue
            }
        }
    }

    // find the cell on the bottom row with the greatest distance
    int maxDist = 0;
    Point maxCell;
    int y = height - 1;
    for (int x = 0; x < width; x ++) {
        if (d[y][x] > maxDist) {
            maxDist = d[y][x];
            maxCell = Point(x,y);
        }
    }

    // starting at the end, use the previous cell to add
    // the appropriate move until we get back to the origin
    Point curr = maxCell;
    while (curr != Point(0,0)) {
        Point prev = p[curr.y][curr.x];
        if (prev.x == curr.x - 1)   solution.push_back(RIGHT);
        if (prev.y == curr.y - 1)   solution.push_back(DOWN);
        if (prev.x == curr.x + 1)   solution.push_back(LEFT);
        if (prev.y == curr.y + 1)   solution.push_back(UP);
        curr = prev;
    }

    // we added the moves backwards, so reverse the array
    reverse(solution.begin(), solution.end());

    return solution;
}

PNG* SquareMaze::drawMaze() const {
    PNG* png = new PNG(width*10 + 1, height*10 + 1);

    // set the top row to black, except (1,0) thru (9,0)
    for (unsigned x = 10; x < png->width(); x ++) {
        png->getPixel(x,0).l = 0;
    }
    // set the left edge to black
    for (unsigned y = 0; y < png->height(); y ++) {
        png->getPixel(0,y).l = 0;
    }

    // loop through every cell in the maze
    for (int y = 0; y < height; y ++) {
        for (int x = 0; x < width; x ++) {
            int index = y * width + x;
            // if the right wall exists...
            if (walls[index] & 0b01) {
                for (int k = 0; k <= 10; k ++) {
                    png->getPixel((x+1)*10, y*10 + k).l = 0;
                }
            }
            // if the bottom wall exists...
            if (walls[index] & 0b10) {
                for (int k = 0; k <= 10; k ++) {
                    png->getPixel(x*10+k, (y+1)*10).l = 0;
                }
            }
        }
    }

    return png;
}

PNG* SquareMaze::drawMazeWithSolution() {
    PNG* png = drawMaze();
    vector<int> solution = solveMaze();

    int x = 5;
    int y = 5;

    for (int dir : solution) {
        for (int i = 0; i < 10; i ++) {
            // set the pixel to red
            png->getPixel(x,y) = HSLAPixel(0,1,0.5,1);
            // move the "cursor" according to the direction
            if (dir == RIGHT) x ++;
            if (dir == DOWN)  y ++;
            if (dir == LEFT)  x --;
            if (dir == UP)    y --;
        }
    }

    // finish up with the last red pixel
    png->getPixel(x,y) = HSLAPixel(0,1,0.5,1);

    // reuse x and y as the coordinates of the destination cell
    x /= 10;
    y = height - 1;

    // set the bottom edge of the destination cell to white
    for (int k = 1; k < 10; k ++) {
        png->getPixel(x*10 + k, (y+1)*10) = HSLAPixel();
    }

	return png;
}

///////////////
// HexMaze
///////////////
HexMaze::HexMaze() { /* Nothing */ }

void HexMaze::makeMaze(int width, int height) {
    this->width = width;
    this->height = height;

    int numCells = width*height;

    walls.clear();
    walls.resize(numCells, ALL_WALLS);

    DisjointSets cells;
    cells.addelements(numCells);

    while (cells.size(0) < numCells) {
        // pick a starting cell
        int x1 = rand() % width;
        int y1 = rand() % height;

        // default to a no movement
        int dir = -1;
        // special cases for rightmost column:
        //   - bottom right cell can't go anywhere
        //   - odd rows may only go southeast
        //   - even rows may go southeast or southwest, but not east
        if (x1 == width - 1) {
            if (y1 == height - 1)   dir = -1;
            else if (y1 % 2 == 1)   dir = 3;
            else                    dir = rand() % 2 + 1;
        }
        // special cases for bottom row:
        //   - only able to move east
        else if (y1 == height - 1) {
            dir = 0;
        }
        // special cases for leftmost column:
        //   - even rows may not move southwest
        //   - odd rows may move in any direction
        else if (x1 == 0) {
            if (y1 % 2 == 0)  dir = rand() % 2;
            else                dir = rand() % 3;
        }
        // if none of the special cases apply, move in any direction
        else {
            dir = rand() % 3;
        }

        if (dir != -1) {
            int x2, y2;
            // east
            if (dir == 0) {
                x2 = x1 + 1;
                y2 = y1;
            }
            // southeast
            else if (dir == 1) {
                x2 = x1 + ((y1 % 2 == 0) ? 0 : 1);
                y2 = y1 + 1;
            }
            // southwest
            else {
                x2 = x1 + ((y1 % 2 == 0) ? -1 : 0);
                y2 = y1 + 1;
            }

            // make sure the neighbor is on the grid
            if (x2 < 0 || x2 > width || y2 < 0 || y2 > height) {
                continue;
            }

            int index1 = y1 * width + x1;
            int index2 = y2 * width + x2;
            if (cells.find(index1) != cells.find(index2)) {
                cells.setunion(index1, index2);

                switch (dir) {
                    case 0: walls[index1] &= 0b110; break;
                    case 1: walls[index1] &= 0b101; break;
                    case 2: walls[index1] &= 0b011; break;
                }
            }
        }
    }
}

bool HexMaze::canTravel(int x, int y, int dir) const {
    // if moving up or left, adjust the cell coordinate
    // and reverse the direction, essentially checking from the other cell
    switch (dir) {
        case WEST:
            x --;
            dir = EAST;
            break;
        case NORTHWEST:
            if (y % 2 == 0) x --;
            y --;
            dir = SOUTHEAST;
            break;
        case NORTHEAST:
            if (y % 2 == 1) x ++;
            y --;
            dir = SOUTHWEST;
            break;
    }

    // make sure the cell we want exists
    if (x < 0 || x >= width || y < 0 || y >= width)
        return false;

    // mask out the wall we are interested in and return true if it doesn't exist
    if      (dir == EAST)       return !(walls[y*width + x] & 0b001);
    else if (dir == SOUTHEAST)  return !(walls[y*width + x] & 0b010);
    else                        return !(walls[y*width + x] & 0b100);


}

vector<int> HexMaze::solveMaze() {
    vector<int> solution;

    // matrix of visited cells
    vector<vector<bool>> v(height, vector<bool>(width,false));
    // matrix of cell distance from origin
    vector<vector<int>>  d(height, vector<int>(width,0));
    // matrix of previous cell coordinates
    vector<vector<Point>> p(height, vector<Point>(width, Point(-1,-1)));

    queue<Point> q;
    v[0][0] = true;
    q.push(Point(0,0));

    while (!q.empty()) {
        Point cell = q.front(); q.pop();
        int x = cell.x;
        int y = cell.y;
        int dist = d[y][x];

        // check for adjacent cells -> starting with east going clockwise
        vector<Point> adj;
        if (canTravel(x, y, EAST)) {
            adj.push_back(Point(x + 1, y));
        }
        if (canTravel(x, y, SOUTHEAST)) {
            if (y % 2 == 0) adj.push_back(Point(x, y + 1));
            else            adj.push_back(Point(x + 1, y + 1));
        }
        if (canTravel(x, y, SOUTHWEST)) {
            if (y % 2 == 0) adj.push_back(Point(x - 1, y + 1));
            else            adj.push_back(Point(x, y + 1));
        }
        if (canTravel(x, y, WEST)) {
            adj.push_back(Point(x - 1, y));
        }
        if (canTravel(x, y, NORTHWEST)) {
            if (y % 2 == 0) adj.push_back(Point(x - 1, y - 1));
            else            adj.push_back(Point(x, y - 1));
        }
        if (canTravel(x, y, NORTHEAST)) {
            if (y % 2 == 0) adj.push_back(Point(x, y - 1));
            else            adj.push_back(Point(x + 1, y - 1));
        }

        // for each adjacent cell
        for (Point c : adj) {
            x = c.x;
            y = c.y;
            // if it hasn't been visited
            if (v[y][x] == false) {
                p[y][x] = cell;         // set the previous coordinate,
                d[y][x] = dist + 1;     // increment the distance,
                v[y][x] = true;         // record that we visited,
                q.push(c);              // and add to the BFS queue
            }
        }
    }

    // find the cell on the bottom row with the greatest distance
    int maxDist = 0;
    int y = height - 1;
    endCell = Point(0,y);
    for (int x = 0; x < width; x ++) {
        if (d[y][x] > maxDist) {
            maxDist = d[y][x];
            endCell = Point(x,y);
        }
    }

    // starting at the end, use the previous cell to add
    // the appropriate move until we get back to the origin
    Point curr = endCell;
    while (curr != Point(0,0)) {
        Point prev = p[curr.y][curr.x];

        // even rows
        if (curr.y % 2 == 0) {
            if (prev.x == curr.x - 1) {
                if (prev.y == curr.y - 1)         solution.push_back(SOUTHEAST);
                else if (prev.y == curr.y + 1)    solution.push_back(NORTHEAST);
                else                              solution.push_back(EAST);
            }
            else if (prev.x == curr.x) {
                if (prev.y == curr.y - 1)         solution.push_back(SOUTHWEST);
                else                              solution.push_back(NORTHWEST);
            }
            else                                  solution.push_back(WEST);
        }
        // odd rows
        else {
            if (prev.x == curr.x + 1) {
                if (prev.y == curr.y - 1)         solution.push_back(SOUTHWEST);
                else if (prev.y == curr.y + 1)    solution.push_back(NORTHWEST);
                else                              solution.push_back(WEST);
            }
            else if (prev.x == curr.x) {
                if (prev.y == curr.y - 1)         solution.push_back(SOUTHEAST);
                else                              solution.push_back(NORTHEAST);
            }
            else                                  solution.push_back(EAST);

        }
        curr = prev;
    }

    reverse(solution.begin(), solution.end());

    return solution;
}

PNG* HexMaze::drawMaze() const {
    int w = (int) (hexWidth * (width + 0.5));
    int h = (int) (hexHeight * (height * 0.75 + 0.25));
    PNG* png = new PNG(w + 40, h + 40);

    // set the background
    for (unsigned x = 0; x < png->width(); x ++) {
        for (unsigned y = 0; y < png->height(); y ++) {
            png->getPixel(x,y) = HSLAPixel(216, 0.6, 0.18);
        }
    }

    // draw the grid
    drawGrid(png, 20);

    return png;
}

PNG* HexMaze::drawMazeWithSolution() {
    PNG* png = drawMaze();
    vector<int> solution = solveMaze();

    // start in the center of the top left cell
    double x = 20 + hexWidth / 2;
    double y = 20 + hexHeight / 2;

    for (int dir : solution) {
        double x2, y2;
        switch (dir) {
            case EAST:
                x2 = x + hexWidth;
                y2 = y;
                break;
            case SOUTHEAST:
                x2 = x + hexWidth / 2;
                y2 = y + hexHeight * 0.75;
                break;
            case SOUTHWEST:
                x2 = x - hexWidth / 2;
                y2 = y + hexHeight * 0.75;
                break;
            case WEST:
                x2 = x - hexWidth;
                y2 = y;
                break;
            case NORTHWEST:
                x2 = x - hexWidth / 2;
                y2 = y - hexHeight * 0.75;
                break;
            case NORTHEAST:
                x2 = x + hexWidth / 2;
                y2 = y - hexHeight * 0.75;
                break;
        }

        drawLine(png, HSLAPixel(), (int)(x+0.5), (int)(y+0.5), (int)(x2+0.5), (int)(y2+0.5));
        x = x2;
        y = y2;
    }

    return png;
}

/////////////////////////////////
// Private Helper Functions
/////////////////////////////////
void HexMaze::drawLine(PNG* image, HSLAPixel color, Point p0, Point p1) const {
    drawLine(image, color, p0.x, p0.y, p1.x, p1.y);
}

void HexMaze::drawLine(PNG* image, HSLAPixel color, int x0, int y0, int x1, int y1) const {
    if (abs(y1 - y0) < abs(x1 - x0)) {
        if (x0 > x1)    drawLineLow(image, color, x1, y1, x0, y0);
        else            drawLineLow(image, color, x0, y0, x1, y1);
    }
    else {
        if (y0 > y1)    drawLineHigh(image, color, x1, y1, x0, y0);
        else            drawLineHigh(image, color, x0, y0, x1, y1);
    }
}

void HexMaze::drawLineLow(PNG* image, HSLAPixel color, int x0, int y0, int x1, int y1) const {
    double dx = x1 - x0;
    double dy = y1 - y0;
    int y_inc = 1;
    if (dy < 0) {
        y_inc = -1;
        dy = -dy;
    }
    double D = 2 * dy - dx;
    int y = y0;

    for (int x = min(x0, x1); x < max(x0, x1); x ++) {
        image->getPixel(x,y) = color;
        if (D > 0) {
            y = y + y_inc;
            D = D - 2 * dx;
        }
        D = D + 2 * dy;
    }
}

void HexMaze::drawLineHigh(PNG* image, HSLAPixel color, int x0, int y0, int x1, int y1) const {
    double dx = x1 - x0;
    double dy = y1 - y0;
    int x_inc = 1;
    if (dx < 0) {
        x_inc = -1;
        dx = -dx;
    }
    double D = 2 * dx - dy;
    int x = x0;

    for (int y = min(y0, y1); y < max(y0, y1); y ++) {
        image->getPixel(x,y) = color;
        if (D > 0) {
            x = x + x_inc;
            D = D - 2 * dy;
        }
        D = D + 2 * dx;
    }
}

void HexMaze::drawGrid(PNG* image, int borderWidth) const {
    for (int row = height - 1; row >= 0; row --) {
        for (int col = width - 1; col >= 0; col --) {
            int cellIndex = row * width + col;

            // calculate the center location for this cell
            int centerX = borderWidth + (int) ((col + 1) * hexWidth - ((row % 2) ? 0 : hexWidth / 2));
            int centerY = borderWidth + (int) (hexHeight / 2 + row * 0.75 * hexHeight);

            bool edges[6] = {false};

            // check the walls for this cell
            for (int i = 0; i < 6; i ++) {
                bool isWall = (walls[cellIndex] >> i) & 1;
                if (isWall) edges[i] = true;
            }

            // special cases for cells along the perimeter
            // top row
            if (row == 0) {
                edges[4] = edges[5] = true;
            }
            // left column
            if (col == 0) {
                edges[3] = true;
                if (row % 2 == 0) edges[4] = true;
            }
            // right column
            if (col == width - 1) {
                if (row % 2 == 1) edges[5] = true;
            }

            fillHexagon(image, Point(centerX, centerY), hexSize, edges, HSLAPixel(11, 0.81, 0.53), HSLAPixel(0,1,0));
        }
    }
}

void HexMaze::fillHexagon(PNG* image, Point loc, int size, bool edges[6], HSLAPixel bg, HSLAPixel fg) const {
    double width = sqrt(3) * size;
    double height = 2 * size;

    // calculate a bounding box for the cell to avoid iterating over the whole image
    int minX = (int) (loc.x - width / 2);
    int maxX = (int) (loc.x + width / 2 + 0.5);
    int minY = loc.y - size;
    int maxY = loc.y + size;

    Hexagon hex = Hexagon(loc, size);

    // fill the interior of the hexagon
    for (int y = minY; y <= maxY; y ++) {
        for (int x = minX; x <= maxX; x ++) {
            if (hex.contains(Point(x,y)))
                image->getPixel(x,y) = bg;
        }
    }

    // draw the borders of the cell
    for (int i = 0; i < 6; i ++) {
        int j = (i+1) % 6;
        if (edges[i])   drawLine(image, fg, hex.corners[i], hex.corners[j]);
        else            drawLine(image, bg, hex.corners[i], hex.corners[j]);
    }
}

///////////////
// Point
///////////////
Point::Point() : x(0), y(0) { /* Nothing */ }
Point::Point(int x_, int y_) : x(x_), y(y_) { /* Nothing */ }

bool Point::operator==(const Point& other) { return (x == other.x && y == other.y); }
bool Point::operator!=(const Point& other) { return !(*this == other); }
Point Point::operator+(const Point& other) { return Point(x + other.x, y + other.y); }

///////////////
// Hexagon
///////////////
Hexagon::Hexagon(Point c, int s) : center(c), size(s) {
    for (int i = 0; i < 6; i ++) {
        int x = (int) (center.x + size * cos((2*i - 1) * M_PI / 6.0) + 0.5);   // +0.5 forces nearest-int rounding
        int y = (int) (center.y + size * sin((2*i - 1) * M_PI / 6.0) + 0.5);   // 2i - 1 rotates angle by PI/3 to give pointy-top
        corners.push_back(Point(x,y));
    }
}

bool Hexagon::contains(Point p) {
    // keep track of cross product result
    // if both of these become non-zero, the cross product changed sign
    int pos = 0;
    int neg = 0;

    for (int i = 0; i < 6; i ++) {
        // first check if the point is a corner
        if (corners[i] == p)
            return true;

        // then create a line segment between the i'th and i'th + 1 corner
        Point p1 = corners[i];
        Point p2 = corners[(i+1)%6];

        // compute the cross product
        int d = (p.x - p1.x)*(p2.y - p1.y) - (p.y - p1.y)*(p2.x - p1.x);

        if (d > 0) pos ++;
        if (d < 0) neg ++;

        // if the sign is not consistent, the point is
        // inside some edges and outside others, so it is not in the Hexagon
        if (pos > 0 && neg > 0)
            return false;
    }

    // if we made it out without detecting a change, the point must be inside
    return true;
}
