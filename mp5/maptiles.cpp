/**
 * @file maptiles.cpp
 * Code for the maptiles function.
 */

#include <iostream>
#include <map>
#include "maptiles.h"

using namespace std;

Point<3> convertToLAB(HSLAPixel pixel) {
    Point<3> result(pixel.h/360, pixel.s, pixel.l);
    return result;
}

MosaicCanvas* mapTiles(SourceImage const& theSource,
                       vector<TileImage>& theTiles)
{
    int numRows = theSource.getRows();
    int numCols = theSource.getColumns();
    MosaicCanvas* mosaic = new MosaicCanvas(numRows, numCols);
    map<Point<3>, int> tileAvgs;
    vector<Point<3>> avgsVector;

    int i = 0;
    for (const TileImage& tile : theTiles) {
        HSLAPixel tileAvg = tile.getAverageColor();
        Point<3> avg = convertToLAB(tileAvg);
        avgsVector.push_back(avg);
        tileAvgs.insert(pair<Point<3>, int>(avg, i));
        i++;
    }
    KDTree<3> tree = KDTree<3>(avgsVector);

    for (int x = 0; x < numRows; x++) {
        for (int y = 0; y < numCols; y++) {
            TileImage* image = get_match_at_idx(tree, tileAvgs, theTiles, theSource, x, y);
            mosaic->setTile(x, y, image);
        }
    }

    return mosaic;
}

TileImage* get_match_at_idx(const KDTree<3>& tree,
                                  map<Point<3>, int> tile_avg_map,
                                  vector<TileImage>& theTiles,
                                  const SourceImage& theSource, int row,
                                  int col)
{
    // Create a tile which accurately represents the source region we'll be
    // using
    HSLAPixel avg = theSource.getRegionColor(row, col);
    Point<3> avgPoint = convertToLAB(avg);
    Point<3> nearestPoint = tree.findNearestNeighbor(avgPoint);

    // Check to ensure the point exists in the map
    map< Point<3>, int >::iterator it = tile_avg_map.find(nearestPoint);
    if (it == tile_avg_map.end())
        cerr << "Didn't find " << avgPoint << " / " << nearestPoint << endl;

    // Find the index
    int index = tile_avg_map[nearestPoint];
    return &theTiles[index];

}
